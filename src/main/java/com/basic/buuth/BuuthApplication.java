package com.basic.buuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.basic.buuth.config.DatabaseConfiguration;
import com.basic.buuth.config.RedisConfig;

@SpringBootApplication
@Import({ DatabaseConfiguration.class, RedisConfig.class })
public class BuuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuuthApplication.class, args);
    }
}
