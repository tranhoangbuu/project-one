package com.basic.buuth.service.command;

import org.springframework.beans.factory.annotation.Autowired;

import com.basic.buuth.mapper.CategoryMapper;
import com.basic.buuth.repositoris.CategoryRepository;

public abstract class CommandService {

    @Autowired
    protected CategoryRepository categoryRepository;
    @Autowired
    protected CategoryMapper categoryMapper;
}
