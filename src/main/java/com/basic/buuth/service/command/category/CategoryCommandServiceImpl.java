package com.basic.buuth.service.command.category;

import org.springframework.stereotype.Service;

import com.basic.buuth.entitis.Category;
import com.basic.buuth.service.command.CommandService;
import com.basic.buuth.service.command.category.dto.CreateCategoryDto;

@Service
public class CategoryCommandServiceImpl extends CommandService implements ICategoryCommandService {

    @Override
    public void cmd(CreateCategoryDto cmd) {
        Category entity = categoryMapper.cmd(cmd);
        System.out.println(entity.getEmail());
        System.out.println(entity.getName());

        this.categoryRepository.save(entity);
    }
}
