package com.basic.buuth.service.command.category;

import org.springframework.stereotype.Service;

import com.basic.buuth.service.command.category.dto.CreateCategoryDto;

@Service
public interface ICategoryCommandService {

    void cmd(CreateCategoryDto cmd);
}
