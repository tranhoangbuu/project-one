package com.basic.buuth.service.command.category.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class CreateCategoryDto {

    @JsonProperty("name")
    private String name;

    @JsonProperty("email")
    private String email;

}
