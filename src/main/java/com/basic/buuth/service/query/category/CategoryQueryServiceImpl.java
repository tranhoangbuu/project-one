package com.basic.buuth.service.query.category;

import org.springframework.stereotype.Service;

import java.util.Optional;

import com.basic.buuth.entitis.Category;
import com.basic.buuth.service.query.QueryService;
import com.basic.buuth.service.query.category.dto.CategoryDto;
import com.basic.buuth.service.query.category.dto.CategoryInclude;
import com.basic.buuth.service.query.category.dto.QueryByIdCategory;
import com.basic.buuth.service.query.category.dto.QueryListCategory;

@Service
public class CategoryQueryServiceImpl extends QueryService implements ICategoryQueryService {

    @Override
    public CategoryDto query(QueryListCategory query) {

        return CategoryDto.builder()
            .build();
    }

    @Override
    public CategoryInclude query(QueryByIdCategory query) {
        Optional<Category> categoryOp = categoryRepository.findById(query.getId());
        if (categoryOp.isPresent()) {

            return categoryMapper.query(categoryOp.get());
        }
        return null;
    }
}
