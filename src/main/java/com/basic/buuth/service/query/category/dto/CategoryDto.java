package com.basic.buuth.service.query.category.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategoryDto {
    List<CategoryInclude> content;
}
