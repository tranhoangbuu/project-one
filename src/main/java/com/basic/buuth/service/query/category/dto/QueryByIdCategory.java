package com.basic.buuth.service.query.category.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class QueryByIdCategory {

    private Long id;
}
