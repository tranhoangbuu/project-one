package com.basic.buuth.service.query.category;

import com.basic.buuth.service.query.category.dto.CategoryDto;
import com.basic.buuth.service.query.category.dto.CategoryInclude;
import com.basic.buuth.service.query.category.dto.QueryByIdCategory;
import com.basic.buuth.service.query.category.dto.QueryListCategory;

public interface ICategoryQueryService {

    CategoryDto query(QueryListCategory query);

    CategoryInclude query(QueryByIdCategory query);


}
