package com.basic.buuth.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import com.basic.buuth.entitis.Category;
import com.basic.buuth.service.command.category.dto.CreateCategoryDto;
import com.basic.buuth.service.query.category.dto.CategoryInclude;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CategoryMapper {

    Category cmd(CreateCategoryDto dto);

    CategoryInclude query(Category entity);
}

