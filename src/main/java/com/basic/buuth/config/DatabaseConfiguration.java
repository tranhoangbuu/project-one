package com.basic.buuth.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories("com.basic.buuth.repositoris")
@ComponentScan({
    "com.basic.buuth.*"
})
@EntityScan(basePackages = "com.basic.buuth.entitis")
public class DatabaseConfiguration {

}
