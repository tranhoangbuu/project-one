package com.basic.buuth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.basic.buuth.service.query.category.ICategoryQueryService;
import com.basic.buuth.service.query.category.dto.CategoryDto;
import com.basic.buuth.service.query.category.dto.CategoryInclude;
import com.basic.buuth.service.query.category.dto.QueryByIdCategory;
import com.basic.buuth.service.query.category.dto.QueryListCategory;

@RestController
@RequestMapping("/api")
public class CategoryQueryController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

//    @Autowired
//    private StringRedisTemplate stringRedisTemplate;

//    @Autowired
//    private RedisTemplate redisTemplate;

    @Autowired
    private ICategoryQueryService service;

    @GetMapping(value = "/category")
    public ResponseEntity<CategoryDto> getList() {
        LOGGER.info("");
        CategoryDto result = service.query(QueryListCategory.builder().build());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/category/{id}")
    @Cacheable(value = "singlePost", key = "#id")
    public ResponseEntity<CategoryInclude> getOne(@PathVariable(value = "id") Long id) {
        LOGGER.info("");

    //    stringRedisTemplate.opsForValue().set("aaa", "111");

    //    Category cate = Category.builder()
    //        .email("age")
    //        .name("123")
    //        .build();

    //    ValueOperations<String, Category> operations = redisTemplate.opsForValue();
    //    operations.set("com.neo.x", cate);
    //    operations.set("com.neo.f", cate);
    //    boolean exists=redisTemplate.hasKey("1");
    //    if(exists){
    //        System.out.println("exists is true");
    //    }else{
    //        System.out.println("exists is false");
    //    }
        CategoryInclude result = service.query(
            QueryByIdCategory.builder()
                .id(id)
                .build());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
