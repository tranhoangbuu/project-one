package com.basic.buuth.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.basic.buuth.service.command.category.ICategoryCommandService;
import com.basic.buuth.service.command.category.dto.CreateCategoryDto;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api")
public class CategoryCommandController {

    @Autowired
    private ICategoryCommandService service;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @ApiOperation(value = "TEN API", notes = "Mo Ta API")
    @ApiResponses({
        @ApiResponse(code = 100, message = "100")
    })
    @PostMapping(value = "/category")
    public ResponseEntity<Void> create(@Valid @RequestBody CreateCategoryDto cmd) {

        LOGGER.info(cmd.toString());

        service.cmd(cmd);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
