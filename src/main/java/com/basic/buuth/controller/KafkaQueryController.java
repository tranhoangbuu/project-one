package com.basic.buuth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.basic.buuth.service.command.kafka.ProducerService;

@RestController
@RequestMapping("/api")
public class KafkaQueryController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProducerService producerService;

    @GetMapping(value = "/producer")
    public ResponseEntity<Void> getProducerList() {
        LOGGER.info("");

        producerService.sendMessage("hahahahahahahahah");

        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
