package com.basic.buuth.repositoris;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.basic.buuth.entitis.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
